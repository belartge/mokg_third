#ifndef AFFINE_TRANSFORM_H
#define AFFINE_TRANSFORM_H

Matrix<> Translation(double x, double y)
{
	double T[9] = {
		1, 0, x,
		0, 1, y,
		0, 0, 1 };
	return Matrix<>(3, 3, T);
}

// ����� ��������� ����������� �������, ������������ ������� ������� ��:
// Identity() - ������������� ��;
// Rotation(t) - ������� �� ���� t;
// Rotation(c, s) - ������� �� ����, ������� � ����� �������� ��������������� ��������� c � s;
// Scaling(kx, ky) - ���������������;
// Mapping (��������� ���� ���������) - �� �������, ��� �������� ������ ��������� ����� ������������ ������� Scaling.

// � ���������� ������� ������������ ����������� ������� �� ������.
// �������������� ���� � ������ �������� �������� ������.
// ��������, ����� �������, ��������� ��������� �������,
// � ����������� ���������������� ��������� � �������������.

Matrix<> Identity() {
	double T[9] = {
		1, 0, 0,
		0, 1, 0,
		0, 0, 1 };
	return Matrix<>(3, 3, T);
}


Matrix<> Rotation(double t) {
	double T[9] = {
		cos(t), -sin(t), 0,
		sin(t), cos(t), 0,
		0, 0, 1 };
	return Matrix<>(3, 3, T);
}

Matrix<> Rotation(double c, double s) {
	double sinus = s / sqrt(c * c + s * s);
	double cosinus = c / sqrt(c * c + s * s);
	double T[9] = {
		cosinus, -sinus, 0,
		sinus, cosinus, 0,
		0, 0, 1 };
	return Matrix<>(3, 3, T);
}

Matrix<> Scaling(double kx, double ky) {
	double T[9] = {
		kx, 0, 0,
		0, ky, 0,
		0, 0, 1 };
	return Matrix<>(3, 3, T);
}

Matrix<> MirrorX() {
	double T[9] = {
		1, 0, 0,
		0, -1, 0,
		0, 0, 1 };
	return Matrix<>(3, 3, T);
}

Matrix<> MirrorY() {
	double T[9] = {
		-1, 0, 0,
		0, 1, 0,
		0, 0, 1 };
	return Matrix<>(3, 3, T);
}

Matrix<> Mirror() {
	double T[9] = {
		-1, 0, 0,
		0, -1, 0,
		0, 0, 1 };
	return Matrix<>(3, 3, T);
}


Matrix<> Identity3D() {
	double T[16] = {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	};
	return Matrix<>(4, 4, T);
}

Matrix<> Translation3D(double x, double y, double z)
{
	double T[16] = {
		1, 0, 0, x,
		0, 1, 0, y,
		0, 0, 1, z,
		0, 0, 0, 1
	};
	return Matrix<>(4, 4, T);
}

Matrix<> Rotation3DAxisX(double t) {
	double T[16] = {
		1, 0, 0, 0,
		0, cos(t), -sin(t), 0,
		0, sin(t), cos(t), 0,
		0, 0, 0, 1 };
	return Matrix<>(4, 4, T);
}

Matrix<> Rotation3DAxisZ(double t) {
	double T[16] = {
		cos(t), -sin(t), 0, 0,
		sin(t), cos(t), 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1 };
	return Matrix<>(4, 4, T);
}

Matrix<> Rotation3DAxisY(double t) {
	double T[16] = {
		cos(t), 0, sin(t), 0,
		0, 1, 0, 0,
		-sin(t), 0, cos(t), 0,
		0, 0, 0, 1 };
	return Matrix<>(4, 4, T);
}

Matrix<> Scaling3D(double kx, double ky, double kz) {
	double T[16] = {
		kx, 0, 0, 0,
		0, ky, 0, 0,
		0, 0, kz, 0,
		0, 0, 0, 1 };
	return Matrix<>(4, 4, T);
}

Matrix<> MirrorXY() {
	double T[16] = {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, -1, 0,
		0, 0, 0, 1 };
	return Matrix<>(4, 4, T);
}

Matrix<> MirrorXZ() {
	double T[16] = {
		1, 0, 0, 0,
		0, -1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1 };
	return Matrix<>(4, 4, T);
}

Matrix<> MirrorYZ() {
	double T[16] = {
		-1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1 };
	return Matrix<>(4, 4, T);
}

Matrix<> RotateOnPoint(double t, double x, double y) {
	return Translation(x, y) * Rotation(t) * Translation(-x, -y);
}

Matrix<> MirrorXOnPoint(double x, double y) {
	return Translation(x, y) * MirrorX() * Translation(-x, -y);
}
Matrix<> MirrorYOnPoint(double x, double y) {
	return Translation(x, y) * MirrorY() * Translation(-x, -y);
}
Matrix<> MirrorOnPoint(double x1, double y1, double x2, double y2) {
	double c, d, l,m;
	double x = (x1 + x2) / 2;
	double y = (y1 + y2) / 2;

	l = y2 - y1;
	m = x2 - x1;

	c = m / sqrt(l*l + m * m);
	d = l / sqrt(l*l + m * m);

	return Translation(x1, y1) * Rotation(acos(d)) * MirrorY()  * Rotation(-acos(d))  * Translation(-x1, -y1);
}

Matrix<> ProjectToXY(double D) {
	double T[16] = {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 0, 0,
		0, 0, 1/D, 1 };
	return Matrix<>(4, 4, T);
}
Matrix<> ProjectToXZ(double D) {
	double T[16] = {
		1, 0, 0, 0,
		0, 0, 0, 0,
		0, 0, 1, 0,
		0, 1/D, 0, 1 };
	return Matrix<>(4, 4, T);
}
Matrix<> ProjectToYZ(double D) {
	double T[16] = {
		0, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		1/D, 0, 0, 1 };
	return Matrix<>(4, 4, T);
}

Matrix<> OrtoProjectToXY() {
	double T[16] = {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 0, 0,
		0, 0, 0, 1 };
	return Matrix<>(4, 4, T);
}

Matrix<> OrtoProjectToXZ() {
	double T[16] = {
		1, 0, 0, 0,
		0, 0, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1 };
	return Matrix<>(4, 4, T);
}

Matrix<> OrtoProjectToYZ() {
	double T[16] = {
		0, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1 };
	return Matrix<>(4, 4, T);
}

Matrix<> RotationX(double c, double s)
{
	double T[16] = {
		1, 0, 0, 0,
		0, c, -s, 0,
		0, s,  c, 0,
		0, 	    0,       0, 1
	};
	return Matrix<>(4, 4, T);
}

Matrix<> RotationY(double t)
{
	double T[16] = {
		cos(t), 0, sin(t), 0,
			 0, 1, 0,       0,
		-sin(t), 0, cos(t),  0,
			 0, 0, 0,       1
	};
	return Matrix<>(4, 4, T);
}

Matrix<> RotationY(double c, double s)
{
	double T[16] = {
		c, 0, s, 0,
			 0, 1, 0,       0,
		-s, 0, c,  0,
			 0, 0, 0,       1
	};
	return Matrix<>(4, 4, T);
}

Matrix<> RotationZ(double t)
{
	double T[16] = {
		cos(t), -sin(t), 0, 0,
		sin(t),  cos(t), 0, 0,
			 0,       0, 1, 0,
			 0, 0, 0, 1
	};
	return Matrix<>(4, 4, T);
}

Matrix<> RotationZ(double c, double s)
{
	double T[16] = {
		c, -s, 0, 0,
		s,  c, 0, 0,
			 0,       0, 1, 0,
			 0, 0, 0, 1
	};
	return Matrix<>(4, 4, T);
}

Matrix<> TaskAff(double angle, double x0, double y0, double z0, double x1, double y1, double z1) {

	//double a = angle;
	Matrix<> rotationFirst = Rotation3DAxisX(asin(sqrt(y1 * y1 + z1 * z1)/sqrt(x1 * x1 + y1 * y1 + z1 * z1)));
	Matrix<> rotationSecond = Rotation3DAxisZ(asin(z1 / sqrt(z1 * z1 + y1 * y1)));

	//double v[12] = {
	//					 cos(a) + (1-cos(a))*x1*x1,(1 - cos(a))*x1*y1 - sin(a) * z1,(1 - cos(a))*x1*z1 + sin(a)*y1,
	//					 (1 - cos(a))*x1*y1 + sin(a)*z1 ,cos(a) + (1 - cos(a))*y1*y1,(1 - cos(a))*y1*z1 - sin(a)*x1,
	//					 (1 - cos(a))*z1*x1 - sin(a)*y1,(1 - cos(a))*z1*y1 + sin(a)*x1,cos(a) + (1 - cos(a))*z1*z1,
	//					 1,1,1};
	//Matrix<> rotationTaskFirst(4, 3, v);

	Matrix<> rotationTaskFirst = Rotation3DAxisZ(angle); 
	//Matrix<> rotationTaskSecond = Rotation3DAxisX(angle);

	Matrix<> irrotationFirst = Rotation3DAxisX(-asin(sqrt(y1 * y1 + z1 * z1) / sqrt(x1 * x1 + y1 * y1 + z1 * z1)));
	Matrix<> irrotationSecond = Rotation3DAxisZ(-asin(z1 / sqrt(z1 * z1 + y1 * y1)));

	return Translation3D(x1, y1, z1) * rotationFirst* rotationSecond*rotationTaskFirst * irrotationSecond * irrotationFirst * Translation3D(-x1, - y1,- z1);
	//return Translation3D(x1, y1, z1) * rotationTaskFirst * Translation3D(-x1, - y1,- z1);

}


#endif AFFINE_TRANSFORM_H
