#ifndef SCENE_3D_H
#define SCENE_3D_H

#include "Camera2D.h"
#include "Model3D.h"
#include "AffineTransform.h"

class Scene3D : public Camera2D
{
private:
	typedef double(*Func)(double);
	Model3D model;
	double step = 2;
public:
	Scene3D(double L, double R, double B, double T) : Camera2D(L, R, B, T)
	{
		model = Model3D();
		model.SetTestSet();
		model.Project(OrtoProjectToXY());
		//	this->B = B;
		//	this->R = R;
		//	this->L = L;
		//	this->T = T;
	}
	void Plot(HDC dc, Func f, bool axes = true)
	{
		if (axes)
			Axes(dc);

		RECT r;
		GetClientRect(WindowFromDC(dc), &r);

		LineTo1(dc, ScreenToWorldX(0), f(ScreenToWorldX(0)));

		HPEN pen = (HPEN)SelectObject(dc, CreatePen(PS_SOLID, 3, RGB(0, 0, 255)));

		for (int pixel = 0; pixel < r.right; pixel++)
		{
			LineTo1(dc, ScreenToWorldX(pixel), f(ScreenToWorldX(pixel)));
		}
		pen = (HPEN)SelectObject(dc, CreatePen(PS_SOLID, 1, RGB(0, 0, 0)));
		// ���������� ������� ������� f
		// ����� ������������ � �������� ��������� ������� f ���������� ��� ������� �������:
		// f(x);
	}

	void Render(HDC dc, bool axes = true)
	{
		if (axes)
			Axes(dc);

		RECT r;
		GetClientRect(WindowFromDC(dc), &r);

		HPEN pen = (HPEN)SelectObject(dc, CreatePen(PS_SOLID, 3, RGB(0, 0, 255)));
		for (int i = 0; i < model.getEdgesCount() - 1; i++) {
			int fromIndex = model.GetEdges()(i + 1, 1);
			int toIndex = model.GetEdges()(i + 1, 2);
			MoveTo(model.getVertexX(fromIndex), model.getVertexY(fromIndex));
			LineTo1(dc, model.getVertexX(toIndex), model.getVertexY(toIndex));
		}
		pen = (HPEN)SelectObject(dc, CreatePen(PS_SOLID, 1, RGB(0, 0, 0)));
	}

	void GoForward() {
		model.Apply(Translation3D(0, step, 0));
	}

	void GoBackward() {
		model.Apply(Translation3D(0, -step, 0));
	}

	void GoRight() {
		model.Apply(Translation3D(step, 0, 0));
	}

	void GoLeft() {
		model.Apply(Translation3D(-step, 0, 0));
	}

	void RotateLeft() {
		model.Apply(Rotation3DAxisX(M_PI / 8));
	}

	void RotateRight() {
		model.Apply(Rotation3DAxisX(-M_PI / 8));
	}

	void RotateForward() {
		model.Apply(Rotation3DAxisY(M_PI / 8));
	}

	void RotateBackward() {
		model.Apply(Rotation3DAxisY(-M_PI / 8));
	}

	void RotateForwardZ() {
		model.Apply(Rotation3DAxisZ(M_PI / 8));
	}

	void RotateBackwardZ() {
		model.Apply(Rotation3DAxisZ(-M_PI / 8));
	}

	void GrowUp() {
		model.Apply(Scaling3D(1.5, 1.5, 1.5));
		step *= 1.5;
	}

	void GrowDown() {
		model.Apply(Scaling3D(0.5, 0.5, 0.5));
		step *= 0.5;
	}

	void UseMirrorX() {
		model.Apply(MirrorYZ());
	}

	void UseMirrorY() {
		model.Apply(MirrorXZ());
	}

	void UseMirrorZ() {
		model.Apply(MirrorXY());
	}

	void RotateOnPointLeft() {
		model.Apply(RotateOnPoint(M_PI / 8, model.getVertexX(12), model.getVertexY(12)));
	}
	void RotateOnPointRight() {
		model.Apply(RotateOnPoint(-M_PI / 8, model.getVertexX(12), model.getVertexY(12)));
	}

	void MirrorOnVertice() {
		double cX = model.getVertexX(5) + model.getVertexX(6);
		cX /= 2;
		double cY = model.getVertexY(5) + model.getVertexY(6);
		cY /= 2;

		model.Apply(MirrorOnPoint(model.getVertexX(5), model.getVertexY(5), model.getVertexX(6), model.getVertexY(6)));
	}

	void RotateOnVertexRight() {
		Matrix<> vertices = model.GetVertices();
		int point1index = 1;
		int point2index = 5;
		double point1[3] = { vertices(1, point1index) / vertices(4, point1index), vertices(2, point1index) / vertices(4, point1index), vertices(3, point1index) / vertices(4, point1index) };
		double point2[3] = { vertices(1, point2index) / vertices(4, point2index), vertices(2, point2index) / vertices(4, point2index), vertices(3, point2index) / vertices(4, point2index) };

		double diff_x = point2[0] - point1[0];
		double diff_y = point2[1] - point1[1];
		double diff_z = point2[2] - point1[2];

		double kat1_1 = diff_x;
		double kat1_2 = diff_y;
		double gip_1 = sqrt(kat1_1 * kat1_1 + kat1_2 * kat1_2);
		double sin1 = kat1_1 / gip_1;
		double cos1 = kat1_2 / gip_1;
		double kat2_2 = gip_1;
		double kat2_1 = diff_z;
		double gip_2 = sqrt(kat2_1 * kat2_1 + kat2_2 * kat2_2);
		double sin2 = kat2_1 / gip_2;
		double cos2 = kat2_2 / gip_2;

		model.Apply(Translation3D(-point1[0], -point1[1], -point1[2]));
		model.Apply(RotationZ(cos1, sin1));
		model.Apply(RotationX(cos2, -sin2));

		model.Apply(RotationY(-M_PI / 8));

		model.Apply(RotationX(cos2, sin2));
		model.Apply(RotationZ(cos1, -sin1));
		model.Apply(Translation3D(point1[0], point1[1], point1[2]));
	}

	void RotateOnVertexLeft() {
		Matrix<> vertices = model.GetVertices();
		int point1index = 1;
		int point2index = 5;
		double point1[3] = { vertices(1, point1index) / vertices(4, point1index), vertices(2, point1index) / vertices(4, point1index), vertices(3, point1index) / vertices(4, point1index) };
		double point2[3] = { vertices(1, point2index) / vertices(4, point2index), vertices(2, point2index) / vertices(4, point2index), vertices(3, point2index) / vertices(4, point2index) };

		double diff_x = point2[0] - point1[0];
		double diff_y = point2[1] - point1[1];
		double diff_z = point2[2] - point1[2];

		double kat1_1 = diff_x;
		double kat1_2 = diff_y;
		double gip_1 = sqrt(kat1_1 * kat1_1 + kat1_2 * kat1_2);
		double sin1 = kat1_1 / gip_1;
		double cos1 = kat1_2 / gip_1;
		double kat2_2 = gip_1;
		double kat2_1 = diff_z;
		double gip_2 = sqrt(kat2_1 * kat2_1 + kat2_2 * kat2_2);
		double sin2 = kat2_1 / gip_2;
		double cos2 = kat2_2 / gip_2;

		model.Apply(Translation3D(-point1[0], -point1[1], -point1[2]));
		model.Apply(RotationZ(cos1, sin1));
		model.Apply(RotationX(cos2, -sin2));

		model.Apply(RotationY(M_PI / 8));

		model.Apply(RotationX(cos2, sin2));
		model.Apply(RotationZ(cos1, -sin1));
		model.Apply(Translation3D(point1[0], point1[1], point1[2]));
	}
	
};

#endif SCENE_3D_H
