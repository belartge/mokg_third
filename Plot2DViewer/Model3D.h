#ifndef MODEL_3D_H
#define MODEL_3D_H
#include <string>
#include <fstream>
#include "Matrix.h"
#include "AffineTransform.h"
class Model3D
{
private:
	Matrix<> Vertices;
	Matrix<int> Edges;
	Matrix<> CumulativeAT;
	Matrix<> InitialVertices;
	Matrix<> ProjectedVertices;
	Matrix<int> Faces;
	bool saveMode = false;
public:
	Model3D() : Vertices(), Edges() {
		SetTestSet();
		CumulativeAT = Identity3D();
		InitialVertices = Vertices;
		
	}
	Model3D(const Matrix<> Vertices, const Matrix<int> Edges) :
		Vertices(Vertices), Edges(Edges) {}
	void SetEdges(Matrix<int> edges) {
		this->Edges = edges;
	}
	Matrix<> GetVertices() { return Vertices; }
	Matrix<int> GetEdges() { return Edges; }
	Matrix<int> GetFaces() { return this->Faces; }

	int getEdgesCount() {
		return Edges.getRowsCount();
	}

	double getVertexX(int index) {
		return ProjectedVertices.operator()(1, index) / ProjectedVertices.operator()(4, index);
	}

	double getVertexY(int index) {
		return ProjectedVertices.operator()(2, index) / ProjectedVertices.operator()(4, index);
	}

	double getVertexZ(int index) {
		return ProjectedVertices.operator()(3, index) / ProjectedVertices.operator()(4, index);
	}

	void Apply(Matrix<> transformation) {
		this->CumulativeAT = transformation * this->CumulativeAT;
		this->Vertices = transformation * this->Vertices;
		Project(OrtoProjectToXY());
	}

	void Project(Matrix<> projection) {
		this->ProjectedVertices = projection * (this->Vertices);
	}

	void SetTestSet() {
		//������
		//double v[21] = { 3,4,5,6,7,5,5,4,4,4,4,4,1,7,1,1,1,1,1,1,1};
		//Matrix<> V(3, 7, v);
		//int e[20] = { 1,7,2,7,3,7,4,7,5,7,1,6,2,6,3,6,4,6,5,6 };
		//Matrix<int> E(10, 2, e);
		double v[20] = { 
						 2,2,-2,-2,0,
						 0,0,0,0,2,
						 2,-2,-2,2,0,
						 1,1,1,1,1 };
		//double v[20] = { 2,2,0,0,0,0,0,0,0,2,2,0,0,2,0,1,1,1,1,1 };
		Matrix<> V(4, 5, v);
		this->Vertices = V;
		this->InitialVertices = V;
		int e[16] = { 1,2, 2,3, 3,4, 4,1, 1,5, 2,5, 3,5, 4,5 };
		Matrix<int> edges(9, 2, e);
		this->Edges = edges;

		CumulativeAT = Identity3D();
	}

	void save() {
		ofstream fout("Saved.txt", std::ios::trunc);
		fout << "3 " << CumulativeAT.getRowsCount() << " ";
		for (int i = 0; i < Vertices.getRowsCount() * 3; i++) {
			fout << CumulativeAT.operator()(1, i + 1) << " ";
		}
	}

	void restore() {
		ifstream fin("Saved.txt");
		if (fin.peek() != std::ifstream::traits_type::eof()) {
			int rows, cols;
			fin >> rows >> cols;
			double * vers = new double[rows*cols];
			for (int i = 0; i < rows*cols; i++) {
				fin >> vers[i];
			}
			Matrix<double> modifications(rows, cols, vers);
			this->CumulativeAT = modifications;

			Apply(CumulativeAT);
		}
	}

	Model3D(string VerticesPath, string EdgesPath) {
		ifstream fin(VerticesPath);
		int rows, cols;
		fin >> rows >> cols;
		double * vers = new double[rows*cols];
		for (int i = 0; i < rows*cols; i++) {
			fin >> vers[i];
		}
		Matrix<double> vertices(rows, cols, vers);
		this->Vertices = vertices;

		ifstream findl(EdgesPath);
		findl >> rows;
		int * edgy = new int[2 * rows];
		for (int i = 0; i < 2 * rows; i++) {
			findl >> edgy[i];
		}
		Matrix<int> edges(rows, 2, edgy);
		this->Edges = edges;

		CumulativeAT = Identity();
		InitialVertices = Vertices;

		if (saveMode) {
			restore();
		}
	}
};
#endif MODEL_3D_H