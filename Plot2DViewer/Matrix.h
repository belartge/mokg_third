#ifndef MATRIX_H
#define MATRIX_H

#include <iostream>

using namespace std;


template <typename Cell = double>
class Matrix
{
private:
	int cols, rows;
	Cell **cells;
	void AllocateCells(int, int);
	void FreeCells();
public:
	Matrix() : cols(0), rows(0), cells(nullptr) {}	// ����������� �� ���������
	Matrix(const Matrix&);					// ����������� �����������
											//Matrix(int);							// ����������� ������� �������
	Matrix(int, int);						// ����������� ������� �������
	Matrix(int, int, Cell*);				// ����������� ������� �� ������
	~Matrix();								// ����������

	Cell &operator()(int i, int j) { return cells[i - 1][j - 1]; }

	Matrix& operator = (const Matrix&);		// ���������� ��������� ������������
	Matrix  operator + (const Matrix&);		// �������� ������
	Matrix  operator - (const Matrix&);		// ��������� ������
	Matrix  operator * (const Matrix&);		// ��������� ������

	int getRowsCount() {
		return rows;
	}

	friend istream& operator >> <> (istream&, Matrix&);			// ���������� ��������� >> ��� ����� �������
	friend ostream& operator << <> (ostream&, const Matrix&);	// ���������� ��������� << ��� ������ �������
};


template <typename Cell>
Matrix<Cell>::Matrix(const Matrix<Cell>& M)
{
	AllocateCells(M.rows, M.cols);
	for (int i = 0; i<rows; i++)
		for (int j = 0; j<cols; j++)
			cells[i][j] = M.cells[i][j];
}

template <typename Cell>
Matrix<Cell>::Matrix(int rows, int cols)
{
	AllocateCells(rows, cols);
	for (int i = 0; i<rows; i++)
		for (int j = 0; j<cols; j++)
			cells[i][j] = 0;
}
template <typename Cell>
Matrix<Cell>::Matrix(int rows, int cols, Cell* list)
{
	AllocateCells(rows, cols);
	int index = 0;
	for (int i = 0; i<rows; i++)
		for (int j = 0; j<cols; j++)
			cells[i][j] = list[index++];
}

template <typename Cell>
Matrix<Cell>::~Matrix()
{
	FreeCells();
}

template <typename Cell>
Matrix<Cell>& Matrix<Cell>::operator=(const Matrix& M)
{
	if (cols != M.cols || rows != M.rows)
	{
		FreeCells();
		AllocateCells(M.rows, M.cols);
	}
	for (int i = 0; i<rows; i++)
		for (int j = 0; j<cols; j++)
			cells[i][j] = M.cells[i][j];
	return *this;
}

template <typename Cell>
Matrix<Cell> Matrix<Cell>::operator+(const Matrix& M)
{
	Matrix<Cell> res(*this);
	if (rows == M.rows && cols == M.cols)
	{
		for (int i = 0; i<rows; i++)
			for (int j = 0; j<cols; j++)
				res.cells[i][j] += M.cells[i][j];
	}
	return res;
}

template <typename Cell>
Matrix<Cell> Matrix<Cell>::operator-(const Matrix& M)
{
	Matrix<Cell> res(*this);
	if (rows == M.rows && cols == M.cols)
	{
		for (int i = 0; i<rows; i++)
			for (int j = 0; j<cols; j++)
				res.cells[i][j] -= M.cells[i][j];
	}
	return res;
}

template <typename Cell>
Matrix<Cell> Matrix<Cell>::operator*(const Matrix& M)
{
	Matrix<Cell> res(rows, M.cols);
	Matrix<Cell> copyOfFirst(*this);

	if (cols == M.rows)
	{
		for (int i = 0; i<rows; i++)
			for (int j = 0; j < M.cols; j++)
			{
				double sum = 0;
				for (int k = 0; k < cols; k++) {
					sum += copyOfFirst.cells[i][k] * M.cells[k][j];
				}
				res.cells[i][j] = sum;
			}

	}
	copyOfFirst.~Matrix();
	return res;
}
template <typename Cell>
void Matrix<Cell>::AllocateCells(int Rows, int Cols)
{
	cells = new Cell*[Rows];
	for (int i = 0; i<Rows; i++)
		cells[i] = new Cell[Cols];
	rows = Rows;
	cols = Cols;
}

template <typename Cell>
void Matrix<Cell>::FreeCells()
{
	for (int i = 0; i<rows; i++)
		delete cells[i];
	//delete cells;
	rows = 0;
}

template <typename Cell>
istream& operator >> (istream& fi, Matrix<Cell>& M)
{
	for (int i = 0; i<M.rows; i++)
		for (int j = 0; j<M.cols; j++)
			fi >> M.cells[i][j];
	return fi;
}

template <typename Cell>
ostream& operator << (ostream& fo, const Matrix<Cell>& M)
{
	for (int i = 0; i<M.rows; i++)
	{
		fo << "  ";
		for (int j = 0; j<M.cols; j++)
			fo << M.cells[i][j] << " \t";
		fo << endl;
	}
	return fo;
}

#endif MATRIX_H
