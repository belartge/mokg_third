#include <windows.h>
#include <windowsx.h>
#include "Data.h"
#include "Scene3D.h"
#include "Matrix.h"
#include "AffineTransform.h"
#include "Model3D.h"

LRESULT _stdcall WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);						// �������� ������� ���������
int _stdcall WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)		// �������� ���������
{
	// ������ ������������ ����� �������� ��������� - �������� ����: ������� ����������� ������� ����� wc, ����� �������� ���� hWnd
	WNDCLASS wc;
	wc.style = CS_OWNDC;
	wc.lpfnWndProc = WndProc;						// ��� ������� ���������, ������������ �� ������ �������
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;						// ������������� ����������, ���������� ����� ������ ���������� ��� ������� ������
	wc.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
	wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)(6);
	wc.lpszMenuName = 0;							// ���� � ������� ������ �����������
	wc.lpszClassName = (LPCSTR)"MainWindowClass";	// ��� �������� ������, ������������ ��� �������� ����������� ����
	RegisterClass(&wc);								// ����������� ������ wc

	HWND hWnd = CreateWindow(						// hWnd - ����������, ���������������� ����; ������� �������� ���� ��������� ���������� hWnd ��������� ���������
		(LPCSTR)"MainWindowClass",					// ��� �������� ������
		(LPCSTR)"Plot2D Viewer",					// ��������� ����
		WS_OVERLAPPEDWINDOW,						// ����� ����
		200,200,400,400,							// ���������� �� ������ ������ �������� ���� ����, ��� ������ � ������
		nullptr,nullptr,hInstance,nullptr);

	ShowWindow(hWnd,nCmdShow);
	UpdateWindow(hWnd);

	// ������ ������������ ����� �������� ��������� - �������� ���� ��������� ��������� ���������, ������� ������� ��������� � ��������� �� ��������������� �����
	MSG msg;
	while(GetMessage(&msg,nullptr,0,0))				// ������� GetMessage �������� �� ������� ��������� � ������� ��� � ��������� msg
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);						// ������� DispatchMessage ��������� ������� � ������������� ������ ������� ���������
	}

	return 0;
}

// � �������� ������ ����������� ������ ���� ���������� ���������� - �������� ������ ������ Scene2D
// ��� ���������� �������� �������������� ����������� ��������� � �������, ������������� � ���� ������
Scene3D scene(L,R,B,T);

LRESULT _stdcall WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)		// ������� ��������� ��������� � ������������ ��� ���������, ������������ ����
{
	switch (msg)
	{
	case WM_PAINT:
	{
		HDC dc = GetDC(hWnd);
		scene.Clear(dc);				// ����� �������������� � ������ Camera2D ������, ����������� �� ������� ������� ������� ���� hWnd
		scene.Render(dc);				// ����� �������������� � ������ Scene2D ������, ����������� �� ������ �����
		ReleaseDC(hWnd, dc);
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}

	case WM_SIZE:
	{
		HDC dc = GetDC(hWnd);
		scene.SetResolution(dc);
		ReleaseDC(hWnd, dc);
		InvalidateRect(hWnd, nullptr, false);
		return 0;
	}

	case WM_LBUTTONDOWN:
	{
		scene.StartDragging(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
		return 0;
	}

	case WM_MOUSEWHEEL:
	{
		if (GetKeyState(VK_CONTROL) & 0x8000)
		{
			POINT P;
			P.x = GET_X_LPARAM(lParam);
			P.y = GET_Y_LPARAM(lParam);
			ScreenToClient(hWnd, &P);
			if (GET_WHEEL_DELTA_WPARAM(wParam) > 0)
			{
				scene.IncreaseSize(P.x, P.y);
			}
			else {
				scene.DecreaseSize(P.x, P.y);
			}
			InvalidateRect(hWnd, nullptr, false);


		}

		return 0;
	}

	case WM_KEYDOWN:
	{
		HDC dc = GetDC(hWnd);


		switch (wParam)
		{
		case VK_LEFT:
		{
			scene.Move(-scene.getStep(), 0);
			break;
		}
		case VK_RIGHT:
		{
			scene.Move(scene.getStep(), 0);
			break;
		}
		case VK_UP:
		{
			scene.Move(0, scene.getStep());
			break;
		}
		case VK_DOWN:
		{
			scene.Move(0, -scene.getStep());
			break;
		}
		case 0x41: {
			//Letter A
			scene.GoLeft(); break;
		}
		case 0x44: {
			//Letter D
			scene.GoRight(); break;
		}
		case 0xBD: {
			//Button -
			scene.GrowDown(); break;
		}
		case 0xBB: {
			//Button +
			scene.GrowUp(); break;
		}
		case 0x49: {
			//Letter I
			scene.RotateForward(); break;
		}
		case 0x4B: {
			//Letter K
			scene.RotateBackward(); break;
		}
		case 0x4A: {
			//Letter J
			scene.RotateLeft(); break;
		}
		case 0x4C: {
			//Letter L
			scene.RotateRight(); break;
		}
		case 0x55: {
			//Letter U
			scene.RotateBackwardZ(); break;
		}
		case 0x4F: {
			//Letter O
			scene.RotateForwardZ(); break;
		}
		case 0x53: {
			//Letter S
			scene.GoBackward(); break;
		}
		case 0x57: {
			//Letter W
			scene.GoForward(); break;
		}
		case 0x5A: {
			//Letter Z
			scene.RotateOnVertexLeft(); break;
		}
		case 0x58: {
			//Letter X
			scene.RotateOnVertexRight(); break;
		}
		case 0x43: {
			//Letter C
			scene.UseMirrorX(); break;
		}
		case 0x46: {
			//Letter V
			scene.UseMirrorY(); break;
		}
		case 0x42: {
			//Letter B
			scene.UseMirrorZ(); break;
		}
	//	case 0x4B: {
	//		//Letter K
	//		scene.RotateOnPointLeft(); break;
	//	}
	//	case 0x4C: {
	//		//Letter C
	//		scene.RotateOnPointRight(); break;
	//	}
	//	case 0x4D: {
	//		//Letter M
	//		scene.MirrorOnVertice(); break;
	//	}
		}

		InvalidateRect(hWnd, nullptr, false);
		scene.Clear(dc);
		return 0;
	}

	case WM_MOUSEMOVE:
	{
		if (scene.IsDragging())
		{
			HDC dc = GetDC(hWnd);
			scene.Drag(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
			scene.Axes(dc);
			InvalidateRect(hWnd, nullptr, false);
		}
		return 0;
	}

	case WM_LBUTTONUP:
	{
		HDC dc = GetDC(hWnd);
		scene.StopDragging();
		scene.Axes(dc);
		return 0;
	}

	case WM_DESTROY:
		{
			PostQuitMessage(0);
			return 0;
		}

	default:
		{
			return DefWindowProc(hWnd,msg,wParam,lParam);
		}
	}
	return 0;
}
