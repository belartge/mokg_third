#ifndef SCENE_2D_H
#define SCENE_2D_H

#include "Camera2D.h"
#include "Model3D.h"
#include "AffineTransform.h"

class Scene2D : public Camera2D
{
private:
	typedef double (*Func)(double);
	Model3D model;
	double step = 2;
public:
	Scene2D(double L, double R, double B, double T) : Camera2D(L, R, B, T)
	{
		model = Model3D();
		model.SetTestSet();
	//	this->B = B;
	//	this->R = R;
	//	this->L = L;
	//	this->T = T;
	}
	void Plot(HDC dc, Func f, bool axes=true)
	{
		if (axes)
			Axes(dc);

		RECT r;
		GetClientRect(WindowFromDC(dc), &r);

		LineTo1(dc, ScreenToWorldX(0), f(ScreenToWorldX(0)));

		HPEN pen = (HPEN)SelectObject(dc, CreatePen(PS_SOLID, 3, RGB(0, 0, 255)));

		for (int pixel = 0; pixel < r.right; pixel++)
		{
			LineTo1(dc, ScreenToWorldX(pixel), f(ScreenToWorldX(pixel)));
		}
		pen = (HPEN)SelectObject(dc, CreatePen(PS_SOLID, 1, RGB(0, 0, 0)));
		// ���������� ������� ������� f
		// ����� ������������ � �������� ��������� ������� f ���������� ��� ������� �������:
		// f(x);
	}

	void Render(HDC dc, bool axes = true)
	{
		if (axes)
			Axes(dc);

		RECT r;
		GetClientRect(WindowFromDC(dc), &r);

		HPEN pen = (HPEN)SelectObject(dc, CreatePen(PS_SOLID, 3, RGB(0, 0, 255)));
		for (int i = 0; i < model.getEdgesCount() - 1; i++) {
			int fromIndex = model.GetEdges()(i+1, 1);
			int toIndex = model.GetEdges()(i+1, 2);
			MoveTo(model.getVertexX(fromIndex), model.getVertexY(fromIndex));
			LineTo1(dc, model.getVertexX(toIndex), model.getVertexY(toIndex));
		}
		pen = (HPEN)SelectObject(dc, CreatePen(PS_SOLID, 1, RGB(0, 0, 0)));
	}

	void GoForward() {
		model.Apply(Translation(0, step));
	}

	void GoBackward() {
		model.Apply(Translation(0, -step));
	}

	void GoRight() {
		model.Apply(Translation(step, 0));
	}

	void GoLeft() {
		model.Apply(Translation(-step, 0));
	}

	void RotateLeft() {
		model.Apply(Rotation(M_PI / 8));
	}

	void RotateRight() {
		model.Apply(Rotation(-M_PI / 8));
	}

	void GrowUp() {
		model.Apply(Scaling(1.5, 1.5));
		step *= 1.5;
	}

	void GrowDown() {
		model.Apply(Scaling(0.5, 0.5));
		step *= 0.5;
	}

	void UseMirrorX() {
		model.Apply(MirrorX());
	}

	void UseMirrorY() {
		model.Apply(MirrorY());
	}

	void UseMirror() {
		model.Apply(Mirror());
	}

//	void RotateOnPointLeft() {
//		model.Apply(RotateOnPoint(M_PI / 8, model.getVerticeX(12), model.getVerticeY(12)));
//	}
//	void RotateOnPointRight() {
//		model.Apply(RotateOnPoint(-M_PI / 8, model.getVerticeX(12), model.getVerticeY(12)));
//	}
//
//	void MirrorOnVertice() {
//		double cX = model.getVerticeX(5) + model.getVerticeX(6);
//		cX /= 2;
//		double cY = model.getVerticeY(5) + model.getVerticeY(6);
//		cY /= 2;
//
//		model.Apply(MirrorOnPoint(model.getVerticeX(5), model.getVerticeY(5), model.getVerticeX(6), model.getVerticeY(6)));
//	}
};

#endif SCENE_2D_H
